# Init Canvas
canv = document.getElementById 'canv' 

canv.width = "400"
canv.height = "600"
ctx = canv.getContext "2d"

pixel = 40

# Tetris Piece shapes. The first point of each piece is [0,0] (implied)
shapes = [
  [[-1, 0], [-1, 1], [0, 1]], # square
  [[1, 0], [0, 1], [-1, 1]],  # s-shape
  [[-1, 0], [0, 1], [1, 1]],  # z-shape
  [[1, 0], [-1, 0], [0, 1]],  # t-shape
  [[1, 0], [2, 0], [-1, 0]],  # rectangle
  [[-1, 0], [1, 0], [1, -1]], # backwards l-shape
  [[-1, 0], [1, 0], [-1, -1]] # l-shape
]

class Piece
  constructor: () ->
    @pos = [Math.floor(board.cols/2), 0]
    @variant = Math.floor Math.random() * shapes.length 
    @shape = shapes[this.variant][..]

  drop: ->
    @pos[1]++

  cellPos: (index) ->
    [@pos[0] + @shape[index][0], @pos[1] + @shape[index][1]]

  checkBound: (step) ->
    for i in [0...@shape.length]
      return true if step <= 0 and @cellPos(i)[0] + step < 0
      return true if step >= 0 and @cellPos(i)[0] + step > board.cols - 1
    return false

  rotate: (back) ->
    switch @variant
      when 0 then return
      when 1, 2
        @shape = if (@turn = !@turn) then @rot() else @rot(true)
      else
        @shape = if back then @rot() else @rot(true)

  rot: (ccw) ->
    sgn = if ccw then -1 else 1
    ([ sgn*b[1], -sgn*b[0] ] for b in @shape)



# The Board
board =
  rows: 15
  cols: 10
  initv: 0
  piece: undefined
  field: undefined 
  activeColor: "#7878e3"
  inactiveColor: "#eee"
  strokeStyle: "#000"
  fillStyle: "#000"

  newpiece: () ->
    @piece = new Piece()

  init: () ->
    @field = ((@initv for i in [1..@rows]) for j in [1..@cols])
    @newpiece()

  drawPiece: () ->
    if @piece
      drawPixel @piece.pos[0], @piece.pos[1], @activeColor
      for i in [0...@piece.shape.length]
        b = @piece.cellPos i
        drawPixel b[0], b[1], @activeColor

  drawBoard: () ->
    for c in [0...@cols]
      for r in [0...@rows]
        drawPixel c, r if @field[c][r] > 0
          
  checkOccupied: ( dx, dy ) ->
    [x, y] = [ @piece.pos[0] + dx, @piece.pos[1] + dy ]
    return true if @field[x][y] > 0 || y >= @rows

    for i in [0...@piece.shape.length]
      p = @piece.cellPos i 
      return true if @field[ p[0]+dx ][ p[1]+dy ] > 0 || p[1]+dy >= @rows

    return false

  digest: () ->
    p = @piece.pos
    @field[ p[0] ][ p[1] ] = 1
    for i in [0...@piece.shape.length]
      p = @piece.cellPos i
      @field[ p[0] ][ p[1] ] = 1

  checkFull: () ->
    for i in [0...@rows]
      linefull = true
      for j in [0...@cols]
        linefull = false if @field[j][i] == 0
      return i if linefull
    return -1

  remove: (row) ->
    for i in [0...@cols]
      col = @field[i]
      # delete the cell from its row in the column and insert a new cell at the top
      col.splice row, 1
      col.unshift 0

# Canvas functions
clear = () ->
  ctx.fillStyle = board.fillStyle
  ctx.fillRect 0, 0, canv.width, canv.height

draw = () ->
  clear()
  board.drawBoard()
  board.drawPiece()

drawPixel = ( i, j, color ) ->
  color = color || board.inactiveColor
  ctx.strokeStyle = board.strokeStyle
  ctx.lineWidth = 4
  ctx.fillStyle = color  
  ctx.fillRect i * pixel, j * pixel, pixel, pixel
  ctx.strokeRect i * pixel, j * pixel, pixel, pixel


board.init()

# Player Control
window.addEventListener "keydown", ( e ) ->
  switch e.keyCode
    when 37
      board.piece.pos[0]-- unless board.piece.checkBound(-1) || board.checkOccupied(-1, 0)
    when 39
      board.piece.pos[0]++ unless board.piece.checkBound(1) || board.checkOccupied(1, 0)
    when 38
      board.piece.rotate()
      board.piece.rotate true if board.piece.checkBound(0) || board.checkOccupied(0, 0)
    when 40
      board.piece.drop() until board.checkOccupied(0, 1)
    else
      # console.log e.keyCode


score = document.getElementById "score"
rscore = 0
multiplier = 0
rows = -1
cols = 0

update = () ->
  if board.piece
    if board.checkOccupied 0, 1 
      board.digest()
      board.newpiece()
      if board.checkOccupied 0, 0
        alert "Game Over!"
        window.clearInterval render
        window.clearInterval compute
      return
    board.piece.drop()

  rows = -1
  multiplier = 0
  while ( row = board.checkFull() ) >= 0 
    board.remove(row)
    multiplier = multiplier * 2 + 1

  rscore += 100 * multiplier
  score.innerHTML = rscore

# Cycle Control
render = setInterval "draw()", 1000/60
compute = setInterval "update()", 600
