var Piece, board, canv, clear, cols, compute, ctx, draw, drawPixel, multiplier, pixel, render, rows, rscore, score, shapes, update;

canv = document.getElementById('canv');

canv.width = "400";

canv.height = "600";

ctx = canv.getContext("2d");

pixel = 40;

shapes = [[[-1, 0], [-1, 1], [0, 1]], [[1, 0], [0, 1], [-1, 1]], [[-1, 0], [0, 1], [1, 1]], [[1, 0], [-1, 0], [0, 1]], [[1, 0], [2, 0], [-1, 0]], [[-1, 0], [1, 0], [1, -1]], [[-1, 0], [1, 0], [-1, -1]]];

Piece = (function() {

  function Piece() {
    this.pos = [Math.floor(board.cols / 2), 0];
    this.variant = Math.floor(Math.random() * shapes.length);
    this.shape = shapes[this.variant].slice(0);
  }

  Piece.prototype.drop = function() {
    return this.pos[1]++;
  };

  Piece.prototype.cellPos = function(index) {
    return [this.pos[0] + this.shape[index][0], this.pos[1] + this.shape[index][1]];
  };

  Piece.prototype.checkBound = function(step) {
    var i, _i, _ref;
    for (i = _i = 0, _ref = this.shape.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
      if (step <= 0 && this.cellPos(i)[0] + step < 0) {
        return true;
      }
      if (step >= 0 && this.cellPos(i)[0] + step > board.cols - 1) {
        return true;
      }
    }
    return false;
  };

  Piece.prototype.rotate = function(back) {
    switch (this.variant) {
      case 0:
        break;
      case 1:
      case 2:
        return this.shape = (this.turn = !this.turn) ? this.rot() : this.rot(true);
      default:
        return this.shape = back ? this.rot() : this.rot(true);
    }
  };

  Piece.prototype.rot = function(ccw) {
    var b, sgn, _i, _len, _ref, _results;
    sgn = ccw ? -1 : 1;
    _ref = this.shape;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      b = _ref[_i];
      _results.push([sgn * b[1], -sgn * b[0]]);
    }
    return _results;
  };

  return Piece;

})();

board = {
  rows: 15,
  cols: 10,
  initv: 0,
  piece: void 0,
  field: void 0,
  activeColor: "#7878e3",
  inactiveColor: "#eee",
  strokeStyle: "#000",
  fillStyle: "#000",
  newpiece: function() {
    return this.piece = new Piece();
  },
  init: function() {
    var i, j;
    this.field = (function() {
      var _i, _ref, _results;
      _results = [];
      for (j = _i = 1, _ref = this.cols; 1 <= _ref ? _i <= _ref : _i >= _ref; j = 1 <= _ref ? ++_i : --_i) {
        _results.push((function() {
          var _j, _ref1, _results1;
          _results1 = [];
          for (i = _j = 1, _ref1 = this.rows; 1 <= _ref1 ? _j <= _ref1 : _j >= _ref1; i = 1 <= _ref1 ? ++_j : --_j) {
            _results1.push(this.initv);
          }
          return _results1;
        }).call(this));
      }
      return _results;
    }).call(this);
    return this.newpiece();
  },
  drawPiece: function() {
    var b, i, _i, _ref, _results;
    if (this.piece) {
      drawPixel(this.piece.pos[0], this.piece.pos[1], this.activeColor);
      _results = [];
      for (i = _i = 0, _ref = this.piece.shape.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
        b = this.piece.cellPos(i);
        _results.push(drawPixel(b[0], b[1], this.activeColor));
      }
      return _results;
    }
  },
  drawBoard: function() {
    var c, r, _i, _ref, _results;
    _results = [];
    for (c = _i = 0, _ref = this.cols; 0 <= _ref ? _i < _ref : _i > _ref; c = 0 <= _ref ? ++_i : --_i) {
      _results.push((function() {
        var _j, _ref1, _results1;
        _results1 = [];
        for (r = _j = 0, _ref1 = this.rows; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; r = 0 <= _ref1 ? ++_j : --_j) {
          if (this.field[c][r] > 0) {
            _results1.push(drawPixel(c, r));
          } else {
            _results1.push(void 0);
          }
        }
        return _results1;
      }).call(this));
    }
    return _results;
  },
  checkOccupied: function(dx, dy) {
    var i, p, x, y, _i, _ref, _ref1;
    _ref = [this.piece.pos[0] + dx, this.piece.pos[1] + dy], x = _ref[0], y = _ref[1];
    if (this.field[x][y] > 0 || y >= this.rows) {
      return true;
    }
    for (i = _i = 0, _ref1 = this.piece.shape.length; 0 <= _ref1 ? _i < _ref1 : _i > _ref1; i = 0 <= _ref1 ? ++_i : --_i) {
      p = this.piece.cellPos(i);
      if (this.field[p[0] + dx][p[1] + dy] > 0 || p[1] + dy >= this.rows) {
        return true;
      }
    }
    return false;
  },
  digest: function() {
    var i, p, _i, _ref, _results;
    p = this.piece.pos;
    this.field[p[0]][p[1]] = 1;
    _results = [];
    for (i = _i = 0, _ref = this.piece.shape.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
      p = this.piece.cellPos(i);
      _results.push(this.field[p[0]][p[1]] = 1);
    }
    return _results;
  },
  checkFull: function() {
    var i, j, linefull, _i, _j, _ref, _ref1;
    for (i = _i = 0, _ref = this.rows; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
      linefull = true;
      for (j = _j = 0, _ref1 = this.cols; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; j = 0 <= _ref1 ? ++_j : --_j) {
        if (this.field[j][i] === 0) {
          linefull = false;
        }
      }
      if (linefull) {
        return i;
      }
    }
    return -1;
  },
  remove: function(row) {
    var col, i, _i, _ref, _results;
    _results = [];
    for (i = _i = 0, _ref = this.cols; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
      col = this.field[i];
      col.splice(row, 1);
      _results.push(col.unshift(0));
    }
    return _results;
  }
};

clear = function() {
  ctx.fillStyle = board.fillStyle;
  return ctx.fillRect(0, 0, canv.width, canv.height);
};

draw = function() {
  clear();
  board.drawBoard();
  return board.drawPiece();
};

drawPixel = function(i, j, color) {
  color = color || board.inactiveColor;
  ctx.strokeStyle = board.strokeStyle;
  ctx.lineWidth = 4;
  ctx.fillStyle = color;
  ctx.fillRect(i * pixel, j * pixel, pixel, pixel);
  return ctx.strokeRect(i * pixel, j * pixel, pixel, pixel);
};

board.init();

window.addEventListener("keydown", function(e) {
  var _results;
  switch (e.keyCode) {
    case 37:
      if (!(board.piece.checkBound(-1) || board.checkOccupied(-1, 0))) {
        return board.piece.pos[0]--;
      }
      break;
    case 39:
      if (!(board.piece.checkBound(1) || board.checkOccupied(1, 0))) {
        return board.piece.pos[0]++;
      }
      break;
    case 38:
      board.piece.rotate();
      if (board.piece.checkBound(0) || board.checkOccupied(0, 0)) {
        return board.piece.rotate(true);
      }
      break;
    case 40:
      _results = [];
      while (!board.checkOccupied(0, 1)) {
        _results.push(board.piece.drop());
      }
      return _results;
      break;
  }
});

score = document.getElementById("score");

rscore = 0;

multiplier = 0;

rows = -1;

cols = 0;

update = function() {
  var row;
  if (board.piece) {
    if (board.checkOccupied(0, 1)) {
      board.digest();
      board.newpiece();
      if (board.checkOccupied(0, 0)) {
        alert("Game Over!");
        window.clearInterval(render);
        window.clearInterval(compute);
      }
      return;
    }
    board.piece.drop();
  }
  rows = -1;
  multiplier = 0;
  while ((row = board.checkFull()) >= 0) {
    board.remove(row);
    multiplier = multiplier * 2 + 1;
  }
  rscore += 100 * multiplier;
  return score.innerHTML = rscore;
};

render = setInterval("draw()", 1000 / 60);

compute = setInterval("update()", 600);